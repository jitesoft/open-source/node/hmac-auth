import Log from '@jitesoft/yolog/node';

// Add part of the system is only possible to use with the master key.
// This is due to the fact that we don't want to allow all entries to
// be able to add or remove keys.
// So in this case, the endpoint is using a specific authentication (for now).
export default async (request, response, keys) => {
  Log.debug('Remove key called.');
  const publicToRemove = request.body.public;
  delete keys.KeyChain[publicToRemove];
  await keys.save(process.env.KEY_FILE);

  Log.debug(`${publicToRemove} removed`);
  return response.status(204).json({
    status: 'deleted'
  });
};
