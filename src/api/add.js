import Crypto from 'crypto';
import validate from '../security/validateHash';
import Log from '@jitesoft/yolog/node';

// Add part of the system is only possible to use with the master key.
// This is due to the fact that we don't want to allow all entries to
// be able to add or remove keys.
// Hence the endpoint is using a specific authentication (for now).
export default async (request, response, keyChain) => {
  // Check hmac.
  const authHeader = request.getHeader('AUTHORIZATION');
  const bearer = authHeader.split(':').trim();
  const auth = JSON.parse(Buffer.from(bearer).toString('ascii'));

  // Check against the master key.
  const compareTo = {
    public: 'master',
    secret: keyChain.Keys['master'],
    time: auth.time
  };

  let result = false;
  try {
    result = await validate(auth.secret, auth.time, compareTo, keyChain.Keys['master']);
  } catch (err) {
    Log.error(err.message);
  }

  if (!result) {
    return response.status(401).json({
      error: 'Invalid secret key.'
    });
  }

  const newPublic = request.body.public;
  keyChain.Keys[newPublic] = Crypto.randomBytes(64).toString('hex');
  await keyChain.save(process.env.KEY_FILE);

  return response.status(201).json({
    status: 'created',
    public: newPublic,
    secret: keyChain.Keys[newPublic]
  });
};
