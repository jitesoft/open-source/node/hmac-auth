import Scrypt from './scrypt';
import { createDecipheriv, createCipheriv } from 'crypto';
import { promises as fs } from 'fs';
import { lock, unlock } from 'proper-lockfile';
import Log from '@jitesoft/yolog/node';

export default class KeyChain {
  Keys = {};
  #masterKey;
  #algorithm = 'aes-192-cbc';

  async initialize (masterKey, salt) {
    this.#masterKey = await Scrypt(masterKey, salt ?? 'DEFAULT', 24);
  }

  async load (path) {
    await lock(path, {
      retries: 3,
      onCompromised: () => {
        throw new Error('Failed to lock file for read.');
      }
    });

    let fileData = await fs.readFile(path);
    fileData = JSON.parse(fileData.toString('utf8'));
    await unlock(path);

    Object.keys(fileData).forEach((key) => {
      const deCipher = createDecipheriv(this.#algorithm, this.#masterKey, Buffer.alloc(16, 0));
      let decryptedData = deCipher.update(fileData[key], 'hex', 'utf8');
      decryptedData += deCipher.final('utf8');
      this.Keys[key] = decryptedData;
      Log.debug(`Loaded key for ${key} successfully.`);
    });
  }

  async save (path) {
    const result = {};
    Object.keys(this.Keys).forEach((key) => {
      // Create new cipher for each entry saved.
      const cipher = createCipheriv(this.#algorithm, this.#masterKey, Buffer.alloc(16, 0));
      // The auth key is only used to add/remove new keys, not to actually authorize in the API.
      let authKeyData = cipher.update(this.Keys[key], 'utf8');
      authKeyData += cipher.final('hex');
      result[key] = authKeyData;
    });

    const fileData = JSON.stringify(result);

    await lock(path, {
      retries: 3,
      onCompromised: () => {
        throw new Error('Failed to lock file for write.');
      }
    });
    await fs.writeFile(path, fileData, {
      encoding: 'utf8'
    });

    await unlock(path);
  }
}
