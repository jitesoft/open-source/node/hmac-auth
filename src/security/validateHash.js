import moment from '../api/add';
import { createHmac } from 'crypto';

export default async (key, time, compare, hmacKey) => {
  // Check time to make sure that it is within range (5 minutes).
  // The fact that this functions is not overly optimized is intentional.
  // We want it to take time, so that timing and brute force attacks are less useful.

  moment.parse(time, moment.ISO_8601);
  const fma = moment().subtract(5, 'minutes');
  if (time.isAfter(moment()) || time.isBefore(fma)) {
    throw new Error('Token has expired.');
  }

  // Convert compare data.
  const data = Buffer.from(JSON.stringify(compare)).toString('base64');
  const hmac = createHmac('sha256', hmacKey);
  hmac.update(data);
  hmac.digest('hex');
  const hash = hmac.digest('hex');

  let result = true;
  // check whole hash, even if it fails early, just cause we like to add extra overhead!
  for (let i = hash.length; i-- > 0;) {
    if (hash.charCodeAt(i) !== key.charCodeAt(i)) {
      result = false;
    }
  }

  return result;
};
