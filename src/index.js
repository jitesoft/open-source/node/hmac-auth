import Express from 'express';
import auth from './api/auth';
import add from './api/add';
import remove from './api/remove';
import authMiddleware from './authMiddleware';
import init from './initialize';
import Log from '@jitesoft/yolog/node';

init().then((keyChain) => {
  const app = Express();

  app.set('title', 'HMAC Authentication Service.');
  app.set('trust proxy', true);
  app.set('x-powered-by', false);

  app.all('/auth', async (req, res) => auth(req, res, keyChain));
  app.post('/key', async (req, res) => add(req, res, keyChain));
  app.put('/key', async (req, res) => add(req, res, keyChain));
  app.delete('/key', async (req, res) => remove(req, res, keyChain));
  app.get('/key', (r, re) => re.status(404));

  app.use('/key', async (req, res, next) => authMiddleware(req, res, next));

  app.listen(1985, () => {
    Log.info('Server up and running!');
  });
}).catch(e => Log.error(e.message));
