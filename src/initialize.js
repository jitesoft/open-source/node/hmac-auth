import { promises as fs } from 'fs';
import Crypto from 'crypto';
import KeyChain from './security/KeyChain';
import Log from '@jitesoft/yolog/node';

const fileExists = async (path) => {
  try {
    const result = await fs.access(path);
    return true;
  } catch (e) {
    return false;
  }
};

export default async () => {
  // Depending on if there is a master key in the env or not, a new master key have to be created or fetched.
  Log.info('Running initialization process.');
  let masterKey = process.env.MASTER_KEY;
  if (!process.env.MASTER_KEY) {
    Log.info('No master key found in env variables.');
    // Check if a key exists at the location.
    if (await fileExists(process.env.MASTER_KEY_FILE)) {
      Log.info('Found master key file.');
      masterKey = (await fs.readFile(process.env.MASTER_KEY_FILE)).toString('utf8');
    } else {
      Log.info('Generating new master key.');
      masterKey = Crypto.randomBytes(256).toString('hex');
      await fs.writeFile(process.env.MASTER_KEY_FILE, masterKey, {
        encoding: 'utf8'
      });
    }
  }

  Log.debug('Creating Key-chain.');
  const keyChain = new KeyChain();
  await keyChain.initialize(masterKey, process.env.SALT);

  // If key file exists we are done, else create a new one.
  Log.info('Checking for key file...');
  if (await fileExists(process.env.KEY_FILE)) {
    Log.info('Found key file, loading key-chain.');
    await keyChain.load(process.env.KEY_FILE);
  } else {
    await fs.writeFile(process.env.KEY_FILE, '');
    Log.info('No key file found, generating new.');
    keyChain.Keys['master'] = process.env.AUTH_KEY;
    await keyChain.save(process.env.KEY_FILE);
    Log.info('Successfully generated new key file, key-chain initialized.');
  }

  Log.info('Setup complete.');
  return keyChain;
};
