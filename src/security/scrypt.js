import { scrypt } from 'crypto';

/**
 * Async scrypt method.
 *
 * @param {String|Buffer} value Value to encrypt.
 * @param {String} salt  Salt to use to encrypt.
 * @param {Number} len   Length of the resulting value.
 *
 * @return {Promise<String>}
 */
export default async (value, salt, len) => {
  return new Promise((resolve, reject) => {
    scrypt(value, salt, len, (e, k) => e ? reject(e) : resolve(k));
  });
};
