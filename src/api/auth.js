import validate from '../security/validateHash';
import Log from '@jitesoft/yolog/node';

export default async (request, response, keyChain) => {
  // Check hmac.
  const authHeader = request.getHeader('AUTHORIZATION');
  const bearer = authHeader.split(':').trim();
  const auth = JSON.parse(Buffer.from(bearer).toString('ascii'));

  // Check against the master key.
  const compareTo = {
    public: keyChain.Keys[auth.public],
    secret: auth.public,
    time: auth.time
  };

  let result = false;
  try {
    result = await validate(auth.secret, auth.time, compareTo, keyChain.Keys[auth.public]);
  } catch (err) {
    Log.error(err.message);
  }

  if (!result) {
    return response.status(401).json({
      error: 'Invalid secret key.'
    });
  }

  return response.status(200).json({
    status: 'Ok'
  });
};
